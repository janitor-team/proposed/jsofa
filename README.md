JSOFA
=====

[![Build Status](https://travis-ci.org/javastro/jsofa.svg?branch=master)](https://travis-ci.org/javastro/jsofa)

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/org.javastro/jsofa/badge.svg)](https://maven-badges.herokuapp.com/maven-central/org.javastro/jsofa/)

This is a java translation of the official IAU SOFA library.

For more information see <http://javastro.github.com/jsofa/>

